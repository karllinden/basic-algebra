\documentclass[a4paper,12pt]{article}

\usepackage{algorithm}
\usepackage{amsfonts}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{booktabs}
\usepackage{color}
\usepackage{enumerate}
\usepackage{graphicx}
\usepackage{longtable}
\usepackage{mathtools}
\usepackage{hyperref}
\usepackage{pdflscape}
\usepackage{sectsty}
\usepackage{textcomp}
\usepackage{tocloft}

\usepackage[english]{babel}
\usepackage[T1]{fontenc}
\usepackage[latin1]{inputenc}

\newcommand{\inv}[1]{#1^{-1}}
\newcommand{\ssep}{\;|\;}

\begin{document}

\title{Solutions for Basic Algebra}
\author{
  Karl \textsc{Lind�n} \\
  <karl.j.linden@gmail.com> \\
}
\maketitle

\begin{center}
\vfill
{\small \includegraphics[width=2cm]{ccby}

Copyright {\textcopyright} 2014-2015 Karl Lind�n.
This work is licensed under the Creative Commons Attribution 4.0
International License. To view a copy of this license, visit
\url{http://creativecommons.org/licenses/by/4.0/}.}

\vspace{0.5cm}

The entire project can be downloaded from
\url{https://gitlab.com/lilrc/basic-algebra}.

\end{center}
\clearpage

\renewcommand{\cftsecleader}{\cftdotfill{\cftdotsep}}
\tableofcontents
\clearpage

\section{Basic Algebra I}

\setcounter{subsection}{-1}
\subsection{Introduction: Concepts from Set Theory. The Integers}
\subsubsection{The power set of a set}
No exercises.

\subsubsection{The Cartesian product set. Maps}
\begin{enumerate}
  \item
    Let \(\alpha: S \ni s \mapsto \lfloor \frac{1}{2} s \rfloor \in S\)
    and \(\beta: S \ni s \mapsto 2s \in S\).
    Then \(\alpha\beta = 1_S\)
    because
    \[
      \alpha(\beta(s))
        = \alpha(2s)
        = \lfloor \frac{1}{2} \cdot 2s \rfloor
        = \lfloor s \rfloor
        = s
    \]
    for all \(s \in S\), but \(\beta\alpha \ne 1_S\) since
    \[ \beta(\alpha(3)) = \beta(1) = 2 \ne 3. \]

    This is not possible if \(\alpha\) is bijective, because then
    \[
      \alpha\beta = 1_S
        \implies \beta = \inv{\alpha}
        \implies \beta \alpha = \inv{\alpha} \alpha = 1_S.
    \]

  \item
    First the injectivity.
    It shall be shown that
    \[
      \alpha \text{ is injective} \iff
      \exists \beta: T \to S: \beta \alpha = 1_S
    \]

    First assume \(\alpha\) is injective.
    It shall be shown that there exists a map \(\beta\) such that
    \(\beta \alpha = 1_S\).
    Let \(s' \in S\) and set
    \[
      \beta
        = \{(\alpha(s),s) \ssep s \in S\}
          \cup \{(t,s') \ssep t \in T \setminus \alpha(S)\}
    \]
    Clearly, \(T = \alpha(S) \cup (T \setminus \alpha(S))\), so for any
    \(t \in T\) there is an \(s \in S\) such that \((t,s) \in \beta\).
    To prove that \(\beta\) indeed is a map, assume that
    \((t,s_1) \in \beta\) and \((t,s_2) \in \beta\).
    If \(t \in T \setminus \alpha(S)\) then \(t \ne \alpha(s)\) for all
    \(s \in S\).
    Thus,
    \[ (t,s_1) = (t,s') = (t,s_2) \implies s_1 = s_2. \]
    Otherwise \(t \in \alpha(S)\) so that \(t = \alpha(s)\) for some
    \(s \in S\).
    Hence, \(t = \alpha(s_1)\) and \(t = \alpha(s_2)\).
    It follows that \(\alpha(s_1) = \alpha(s_2)\) and injectivity gives
    \(s_1 = s_2\), as desired.

    Secondly assume there is a \(\beta: T \to S\) such that
    \(\beta \alpha = 1_S\).
    Suppose that \(\alpha(s_1) = \alpha(s_2)\).
    Then
    \[ s_1 = \beta(\alpha(s_1)) = \beta(\alpha(s_2)) = s_2, \]
    showing that \(\alpha\) is injective.

    First consider the counter-example
    \[ \alpha: \{1\} \ni 1 \mapsto 1 \in \{1,2\} \]
    The only function \(\{1,2\} \to \{1\}\) is
    \[ \beta: \{1,2\} \ni t \mapsto 1 \in \{1\}. \]
    Thus, there is a unique function \(\beta\) such that
    \(\beta\alpha = 1_S\) but \(\alpha\) is not surjective.

    If \(S\) contains at least two elements, then the above
    counter-example is impossible.
    Suppose \(\alpha\) is not surjective.
    Then \(T \setminus \alpha(S) \ne \varnothing\), so that one can let
    \(\beta_1\) be defined as the above where \(s' = s_1\) and
    \(\beta_2\) be defined analogously for \(s' = s_2 \ne s_1\).
    Then \(\beta_1 \ne \beta_2\), showing that the \(\beta\) is not
    unique.

    Next, the surjectivity.
    It shall be shown thath
    \[
      \alpha: S \to T \text{ is surjective} \iff
      \exists \beta: T \to S \text{ such that } \alpha \beta = 1_T.
    \]

    First suppose \(\alpha\) is surjective.
    Let for each \(t \in T\)
    \[ A_t = \inv{\alpha}(\{t\}) = \{s \in S \ssep \alpha(s) = t\}. \]
    By surjectivity, \(A_t \ne \varnothing\) for all \(t \in T\).
    By the axiom of choice there is a function \(\beta: T \to S\) such
    that \(\beta(t) \in A_t\).
    Now one has
    \[ \alpha(\beta(t)) = t \]
    for all \(t \in T\) showing that \(\alpha \beta = 1_T\).

    Secondly assume there exists \(\beta: T \to S\) such that
    \(\alpha \beta = 1_T\).
    Let \(t \in T\).
    Then
    \[ t = \alpha(\beta(t)) = \alpha(s) \]
    where \(s = \beta(t)\).
    Because \(t\) is arbitrary, \(\alpha\) is surjective.

    Suppose that \(\alpha\) is not injective.
    Then \(t' = \alpha(s_1) = \alpha(s_2)\) for some \(s_1 \ne s_2\).
    Again by the axiom of choice there are functions
    \(\beta_1: T \to S\) and \(\beta_2: T \to S\) such that
    \(\beta_1(t') = s_1\) and \(\beta_2(t') = s_2\).
    (This follows by taking
    \[
      B_t
        = \begin{cases}
          A_t,     &\text{if } t \ne t', \\
          \{s_1\}, &\text{if } t = t',
        \end{cases}
    \]
    and invoking the axiom choice on \(\{B_t\}\) as above to form
    \(\beta_1\) and similarly for \(\beta_2\).)
    Thus, \(\beta\) is not unique.
    By the principle of indirect proof uniqueness implies surjectivity
    and consequently bijectivity.

  \item
  First assume \(\alpha\) is surjective. Since \(\beta_1 \alpha = \beta_2 \alpha\)
  the following is true.
  \[ \beta_1(\alpha(S)) = \beta_2(\alpha(S)), \]
  but since \(\alpha(S) = T\) (surjectivity) this is the same as
  \(\beta_1(T) = \beta_2(T)\), but that is the same as \(\beta_1 = \beta_2\), which
  proves the \(\Rightarrow\).

  If there exist no two maps \(\beta_1,\beta_2\) such that \(\beta_1 \neq \beta_2\),
  and \(\beta_1 \alpha = \beta_2 \alpha\), assume \(\alpha\) is not surjective. Then
  \(\exists t_1 \in T : \forall s \in S : t_1 \neq \alpha(s)\). Now \(\beta_1\) and
  \(\beta_2\) can be chosen such that \(\beta_1(t_1) = u_1\) and
  \(\beta_2(t_1) = u_2\) where \(u_1,u_2 \in U\) and \(u_1 \neq u_2\), but this
  contradicts that there are no two distinct maps \(\beta_1,\beta_2\) which means
  the assumption that \(\alpha\) is not surjective is false. Thus, \(\alpha\)
  must be surjective. This proves the \(\Leftarrow\).

  Together, the \(\iff\) is proven.

  Assume there exist two maps \(\gamma_1, \gamma_2\) such that
  \(\gamma_1 \neq \gamma_2\) and \(\alpha \gamma_1 = \alpha \gamma_2\). This means
  there exists a \(u \in U\) such that \(s_1 = \gamma_1(u)\) and
  \(s_2 = \gamma_2(u)\), where \(s_1 \neq s_2\). For this \(u\)
  \(\alpha(s_1) = \alpha(s_2)\), but this contradicts that \(\alpha\) is
  injective. Hence, the assumption there exist two distinct maps
  \(\gamma_1,\gamma_2\) must be false, which proves the \(\Rightarrow\).

  Assume \(\alpha\) is not injective. Then
  \(\exists s_1, s_2 \in S : s_1 \neq s_2 : \alpha(s_1) = \alpha(s_2)\). This
  means \(\gamma_1, \gamma_2\) can be chosen so that for some \(u \in U\)
  \(\gamma_1(u) = s_1\) and \(\gamma_2(u) = s_2\) still satisfying
  \(\alpha \gamma_1 = \alpha \gamma_2\), but this contradicts that there exist
  no two distinct maps \(\gamma_1, \gamma_2\) such that
  \(\alpha \gamma_1 = \alpha \gamma_2\). Thus the assumption that \(\alpha\) is not
  surjective must be false, which proves the \(\Leftarrow\).

  Together the \(\iff\) is proven.


  \item
  Choose any \(s \in A \cup B\). Then \(s \in A\) or \(s \in B\), thus
  \(\alpha(s) \in \alpha(A)\) or \(\alpha(s) \in \alpha(B)\). This shows that
  \(\alpha(A \cup B) \subseteq \alpha(A) \cup \alpha(B)\). It is obvious that
  \(\alpha(A) \subseteq \alpha(A \cup B)\) and
  \(\alpha(B) \subseteq \alpha(A \cup B)\), which means
  \(\alpha(A) \cup \alpha(B) \subset \alpha(A \cup B)\). This proves
  \(\alpha(A \cup B) = \alpha(A) \cup \alpha(B)\).

  Choose any \(s \in A \cap B\). This is the same as \(s \in A\) and \(s \in B\),
  which means \(\alpha(s) \in \alpha(A)\) and \(\alpha(s) \in \alpha(B)\). This
  proves \(\alpha(A \cap B) \subseteq \alpha(A) \cap \alpha(B)\).

  An example is when \(S = T = \mathbb{R}\), and \(\alpha : x \mapsto x^2\). If
  \(A = [-1,2]\) and \(B = [-2,1]\), then \(\alpha(A) = [0,4]\) and
  \(\alpha(B) = [0,4]\) giving \(\alpha(A) \cap \alpha(B) = [0,4]\), but
  \(A \cap B = [-1,1]\) giving \(\alpha(A \cap B) = [0,1]\).


  \item
  \(\alpha(\sim A) \nsubseteq \sim(\alpha(A))\) is the same as there exists one
  \(\alpha(s)\) such that \(\alpha(s) \in \alpha(\sim A)\), but
  \(\alpha(s) \notin \sim(\alpha(A)) \iff \alpha(s) \in \alpha(A)\). Now if
  \(\alpha\) is not injective there exists two \(s_1\) and \(s_2\) such that
  \(s_1 \neq s_2\), but \(\alpha(s_1) = \alpha(s_2)\). Let \(s_1 \in A\) and
  \(s_2 \notin A \iff s_2 \in \sim A\). Now
  \(\alpha(s) = \alpha(s_1) \in \alpha(A)\), but
  \(\alpha(s) = \alpha(s_2) \in \alpha(\sim A)\). Which fulfills the proof.

  If \(\alpha\) is injective distinct elements in \(A\) have distinct images. Thus
  the images \(\alpha(A)\) and \(\alpha(\sim A)\) must be distinct. Hence,
  \(\alpha(\sim A) \subseteq \sim(\alpha(A))\) where the equality holds if
  \(\alpha\) is surjective.
\end{enumerate}

\subsubsection{Equivalence relations. Factoring a map through an equivalence
               relation}
\begin{enumerate}
    \item
    A partition \(\pi_n(\mathbb{N})\) of \(\mathbb{N}\) is a set of non-vacuous
    subsets of \(\mathbb{N}\) such that the union of the sets in
    \(\pi_n(\mathbb{N})\) is the whole of \(\mathbb{N}\) and distinct sets in
    \(\pi_n(\mathbb{N})\) are disjoint.

    Both cases (i) and (ii) can be generalized and proved together in the
    following way.

    Let
    \[ \pi_n(\mathbb{N}) = \{ A_0, A_1, \dotsc, A_{n-1} \} \]
    where
    \[ A_i = \{i, n + i, 2n + i, \dotsc, kn + i, \dotsc\}. \]

    It is to be proved that \(\pi_n(\mathbb{N})\) is a partition of
    \(\mathbb{N}\) for all \(n \in \mathbb{Z}^+\).

    It is obvious that \(A_i\) is always non-vacuous.

    For any \(n\) it needs to be shown that the union of the sets in
    \(\pi_n(\mathbb{N})\) is the whole \(\mathbb{N}\), which is the same as
    \begin{equation}\label{rE5s}
      \forall m \in \mathbb{N} : m \in A_i.
    \end{equation}
    For \(m = 0\) \eqref{rE5s} is true since \(m = 0 \in A_0\). Assume
    \eqref{rE5s} is true for some \(p \in \mathbb{N}\), which means
    \(p \in A_i \iff \exists k \in \mathbb{N} : p = kn + i\). Now it must be
    that \(p + 1 = kn + i + 1\) if \(i = n - 1\) then
    \(p + 1 = kn + n = (k+1)n\), which means \(p + 1 \in A_0\), otherwise
    \(p + 1 \in A_{i+1}\). Induction proves \eqref{rE5s}.

    Now only the fact that all \(A_i\) and \(A_j\), \(i \neq j\), are disjoint
    needs to be proven to fulfill the proof that \(\pi_n(\mathbb{N})\) is
    a partition. Let \(i \neq j\) and assume
    \(A_i \cap A_j \neq \varnothing\), then there exists a \(q\) such that
    \(q \in A_i\) and \(q \in A_j\). This means \(q = k_1 n + i\) and
    \(q = k_2 n + j\). This gives \((k_1 - k_2) n = j - i\), but since
    \(0 \leq i,j < n\) it must be \(j - i = 0\) (otherwise the right hand
    side is not a multiple of \(n\)) which is the same as \(i = j\). This
    is a contradiction which proves that distinct sets in
    \(\pi_n(\mathbb{N})\) are disjoint.

    The entire definition of a partition is fulfilled, which proves that
    \(\pi_n(\mathbb{N})\) is a partition for all \(n\).

    (i) corresponds to \(n=2\) and (ii) corresponds to \(n=3\) above. Q.E.D.


    \item
    It is clear that \(\sim\) is reflexive since
    \[ (a,b) \sim (a,b) \iff a + b = a + b \]
    is obviously true.

    It is also clear that \(\sim\) is symmetric since
    \[ a + b = c + d \implies c + d = a + b \]
    which proves the symmetry
    \((a,b) \sim (c,d) \implies (c,d) \sim (a,b)\).

    If \((a,b) \sim (c,d)\) and \((c,d) \sim (e,f)\)
    \[ a+b = c+d = e+f \]
    which proves the transitivity.

    Since \(\sim\) is reflexive, symmetric and transitive it is an
    equivalence relation.


    \item
    In the Euclidean plane each point can be represented by an element in
    \(\mathbb{R}^2\). Let
    \[ P : (p_1, p_2) \quad Q : (q_1, q_2) \quad P' : (p'_1, p'_2)
                      \quad Q' : (q'_1. q'_2). \]
    Then \(P,Q,P',Q' \in \mathbb{R}^2\) and \(PQ, P'Q' \in S\).
    \(PQ = (q_1 - p_1, q_2 - p_2)\) and \(P'Q' = (q'_1 - p'_1, q'_2 - p'_2)\).
    Define the relation \(\sim\) as follows
    \[ PQ \sim P'Q' \iff (q_1 - p_1, q_2 - p_2) = (q'_1 - p'_1, q'_2 - p'_2). \]

    The relation is reflexive since
    \[ PQ \sim PQ \iff (q_1 - p_1, q_2 - p_2) = (q_1 - p_1, q_2 - p_2) \]
    is obviously true.

    The relation is also symmetric since
    \[ (q_1 - p_1, q_2 - p_2) = (q'_1 - p'_1, q'_2 - p'_2) \]
    is the same as
    \[ (q'_1 - p'_1, q'_2 - p'_2) = (q_1 - p_1, q_2 - p_2) \]
    which proves the symmetry
    \[ PQ \sim P'Q' \implies P'Q' \sim PQ \]

    Furthermore the transitivity is easily proven since it follows of the
    transitivity of the equality, \(=\). This fulfills the proof that \(\sim\) is
    an equivalence relation and \(\overline{PQ}\) is the vector (equivalence
    class) of \(PQ\).


    \item\label{LN4u}
    First consider the left hand side.
    The set \(ED\) is the set that contains all \((t,v) \in T \times V\) for which
    there exists a \(u \in U\) such that \((t,u) \in D\) and \((u,v) \in E\). \((ED)C\)
    contains all \((s,v) \in S \times V\) for which there exists a \(t \in T\) such
    that \((s,t) \in C\) and \((t,v) \in ED\). This is the same as \((ED)C\) contains
    all \((s,v) \in S \times V\) for which there exists a \((t,u) \in T \times U\)
    such that \((s,t) \in C\), \((t,u) \in D\) and \((u,v) \in E\).

    Next consider the right hand side.
    The set \(DC\) is the set containing all \((s,u) \in S \times U\) for which
    there exists a \(t \in T\) such that \((s,t) \in C\) and \((t,u) \in D\). \(E(DC)\)
    contains all \((s,v) \in S \times V\) for which there exists a \(u \in U\) such
    that \((s,u) \in DC\) and \((u,v) \in E\). This is the same as \(E(DC)\) contains
    all \((s,v) \in S \times V\) for which there exists a \((t,u) \in T \times U\)
    such that \((s,t) \in C\), \((t,u) \in D\) and \((u,v) \in E\).

    It is now clear that the both sides are equal.

    Next is to prove the first half of the identity law \(C 1_S = C\).
    The set \(C 1_S\) is the set containing all \((s',t) \in S \times T\) for which
    there exists a \(s \in S\) such that \((s',s) \in 1_S\) and \((s,t) \in C\). Since
    \(1_S\) only contains elements on the form \((s,s) \in S \times S\) it is clear
    that \(s' = s\) thus \(C 1_S\) is the set containing all \((s,t) \in S \times T\)
    for which there exists a \(s \in S\) such that \((s,s) \in 1_S\) and
    \((s,t) \in C\). \(1_S\) contains all \((s,s) \in S \times S\) and the first
    condition on the set is thus superfluous leaving only the condition
    \((s,t) \in C\) which proves that \(C 1_S = C\)

    The second half of the identity law is proved in a similar manner.
    The set \(1_T C\) is the set containing all \((s,t') \in S \times T\) for which
    there eixsts a \(t \in T\) such that \((s,t) \in C\) and \((t,t') \in 1_T\). Since
    \(1_T\) only contains elements on the form \((t,t) \in T \times T\) it is clear
    that \(t' = t\) thus \(1_T C\) is the set containing all \((s,t) \in S \times T\)
    for which there exists a \(t \in T\) such that \((s,t) \in C\) and
    \((t,t) \in 1_T\). \(1_T\) contains all \((t,t) \in T \times T\) and the second
    condition on the set is thus superfluous leaving only the condition
    \((s,t) \in C\) which proves that \(C = 1_T C\).

    The two sections above prove the entire identity law \(C 1_S = C = 1_T C\).


    \item\label{vH4n}
    First the reflexive property. Assume
    \begin{equation}\label{0whX}
      E \supseteq 1_S.
    \end{equation}
    Then
    \[ \forall s \in S: (s,s) \in 1_S \subseteq E \Rightarrow sEs, \]
    which proves that any set fulfilling \eqref{0whX} is reflexive. Conversely,
    assume \(E\) is reflexive. Then \(\forall s \in S\) \(sEs\), which
    gives
    \[ \forall s \in S: (s,s) \in E \Rightarrow 1_S \subseteq E. \]
    This proves
    \[ E \text{ is reflexive} \iff E \supseteq 1_S. \]

    Secondly the symmetry. Assume
    \begin{equation}\label{1PBN}
      E = E^{-1}.
    \end{equation}
    It is obvious that \(\forall (a,b) \in E \iff aEb\) there exists a
    \((b,a) \in E^{-1} \iff bE^{-1}a\), but since by assumption,
    \(E = E^{-1}\), \(bE^{-1}a\) is the same as \(bEa\). This proves that any set
    fulfilling \eqref{1PBN} fulfills the symmetry \(aEb \Rightarrow bEa\).
    Conversly suppose \(E\) is symmetric. Then for each ordered pair \((a,b) \in E\)
    the inverse \((b,a) \in E^{-1}\) must also be in \(E\). This gives
    \(E^{-1} \subseteq E\). To prove that also \(E \subseteq E^{-1}\) assume
    \(E \nsubseteq E^{-1}\), which means there exists a \((a,b) \in E\) not in
    \(E^{-1}\). Since \((a,b) \notin E^{-1}\) it must also be that \((b,a) \notin E\),
    but this contradicts the assumption that \(E\) is symmetric, thus
    \(E \subseteq E^{-1}\), which together with \(E^{-1} \subseteq E\) gives
    \(E = E^{-1}\). This fulfills the proof of
    \[ E \text{ is symmetric} \iff E = E^{-1} \]

    Lastly the transitivity. Assume
    \begin{equation}\label{55Rn}
      E \supseteq EE.
    \end{equation}
    Then if \((a,b) \in E\) and \((b,c) \in E\), \(EE\) contains \((a,c)\). Since
    \((a,c) \in EE\) and \(EE \subseteq E\), \((a,c) \in E\), which proves that if a
    relation fulfills \eqref{55Rn} it also fulfills the transitivity
    \(aEb \wedge bEc \Rightarrow aEc\). Conversly assume \(E\) is transitive. Then
    for all \((a,b)\) and \((b,c)\) in \(E\), \((a,c) \in E\). Since by definition \(EE\)
    contains all these \((a,c)\) and no other elements, \(EE \subseteq E\). This
    proves
    \[ E \text{ is transitive} \iff E \supseteq EE. \]

    This proves that any relation fulfilling \eqref{0whX}, \eqref{1PBN} and
    \eqref{55Rn} is an equivalence and all equivalence relation fulfills
    \eqref{0whX}, \eqref{1PBN} and \eqref{55Rn}. This fulfills the proof that
    the conditions that a relation \(E\) is an equivalence relation are the
    ones stated above. Q.E.D.


    \item
    Before proving that \(E\) is an equivalence relation a few rules have to be
    proven, namely:
    \begin{equation}\label{6IA1}
      (A \cup B)^{-1} = A^{-1} \cup B^{-1},
    \end{equation}
    \begin{equation}\label{3Z8E}
      (C^r)^{-1} = (C^{-1})^r,
    \end{equation}
    \begin{equation}\label{5VxX}
      A(B_1 \cup B_2) = A B_1 \cup A B_2,
    \end{equation}
    \begin{equation}\label{lm6W}
      (A_1 \cup A_2) B = A_1 B \cup A_2 B.
    \end{equation}
    Proof of \eqref{6IA1}. First assume \((t,s) \in (A \cup B)^{-1}\) then
    \((s,t) \in A\) or \((s,t) \in B\), but this is the same as \((t,s) \in A^{-1}\)
    or \((t,s) \in B^{-1}\) which means \((t,s) \in A^{-1} \cup B^{-1}\). This shows
    that \((A \cup B) \subseteq A^{-1} \cup B^{-1}\). Next assume
    \((t,s) \in A^{-1} \cup B^{-1}\). This means \((t,s) \in A^{-1}\) or
    \((t,s) \in B^{-1}\), which is the same as \((s,t) \in A\) or \((s,t) \in B\).
    This means \((s,t) \in A \cup B\) which implies \((t,s) \in (A \cup B)^{-1}\).
    This shows \(A^{-1} \cup B^{-1} \subseteq (A \cup B)^{-1}\), which together
    with \((t,s) \in A^{-1} \cup B^{-1}\) gives \eqref{6IA1}.

    Proof of \eqref{3Z8E}. Start with \((C^r)^{-1}\), by definition this is equal
    to
    \[
      (C^r)^{-1} = \{ (t,s) \; | \;
      \exists (s_1, s_2, \dotsc, s_{r-1}) \in S^{r-1} :
      sCs_1, s_1Cs_2, \dotsc, s_{r-1}Ct \}.
    \]
    The order of the \((s_1, s_2, \dotsc, s_{r-1})\) can be reversed to
    \((s_{r-1}, s_{r-2}, \dotsc, s_1)\) without changing the meaning. Additionally
    \(aCb \iff bC^{-1}c\). This yields
    \[
      \begin{split}
        (C^r)^{-1} = \{ (t,s) \; &| \;
        \exists (s_{r-1}, s_{r-2}, \dotsc, s_1) \in S^{r-1} : \\
        &\quad tC^{-1}s_{r-1}, s_{r-1}C^{-1}s_{r-2}, \dotsc, s_1C^{-1}s \}.
      \end{split}
    \]
    Note that the right-hand side is by definition equal to \((C^{-1})^r\), which
    proves \eqref{3Z8E}.

    \eqref{5VxX} is easily proven. Assume \(A \subseteq T \times U\) and for
    \(i=1,2\) \(B_i \subseteq S \times T\), then
    \[
      \begin{split}
        A(B_1 \cup B_2)
          &= \{(s,u) \; | \; \exists t \in T :
            (s,t) \in B_1 \cup B_2\wedge (t,u) \in A\} \\
          &= \{(s,u) \; | \; \exists t \in T :
            ((s,t) \in B_1 \vee (s,t) \in B_2) \wedge (t,u) \in A\} \\
          &= \{(s,u) \; | \; \exists t \in T :
            ((s,t) \in B_1 \wedge (t,u) \in A) \vee
            ((s,t) \in B_2 \wedge (t,u) \in A) \} \\
          &= \{(s,u) \; | \; \exists t \in T :
            (s,u) \in AB_1 \vee (s,u) \in AB_2 \} \\
          &= AB_1 \cup AB_2
      \end{split}
    \]
    \eqref{lm6W} is proven similarly.

    Now it can be proven that \(E\) is an equivalence relation. It is just to
    verify that the conditions in exercise \ref{vH4n} hold. It is obvious that
    \(1_S \subseteq E\), since \(E = 1_S \cup (\cdots)\), which shows that \(E\) is
    reflexive. Before continuing define \(C^0 = 1_S\), which makes it possible to
    write \(E\) as
    \[
      E = \bigcup_{k \in \mathbb{N}} (C \cup C^{-1})^k
    \]
    To prove the symmetry consider \(E^{-1}\).
    \[
      E^{-1} = \left( \bigcup_{k \in \mathbb{N}} (C \cup C^{-1})^k \right)^{-1}
    \]
    Repeated usage of \eqref{6IA1} and \eqref{3Z8E} gives
    \[
      \begin{split}
        E^{-1}
          &= \left( \bigcup_{k \in \mathbb{N}} (C \cup C^{-1})^k \right)^{-1}
           = \bigcup_{k \in \mathbb{N}} ((C \cup C^{-1})^k)^{-1} \\
          &= \bigcup_{k \in \mathbb{N}} ((C \cup C^{-1})^{-1})^k
           = \bigcup_{k \in \mathbb{N}} ((C^{-1} \cup (C^{-1})^{-1}))^k \\
          &= \bigcup_{k \in \mathbb{N}} (C^{-1} \cup C)^k
           = \bigcup_{k \in \mathbb{N}} (C \cup C^{-1})^k
           = E
      \end{split}
    \]
    This proves that \(E\) is symmetric. To prove transitivity one can rewrite
    \(EE\) using \eqref{5VxX} and \eqref{lm6W}.
    \[
      \begin{split}
        EE
          &= \left( \bigcup_{i \in \mathbb{N}} (C \cup C^{-1})^i \right)
             \left( \bigcup_{j \in \mathbb{N}} (C \cup C^{-1})^j \right) \\
          &= \bigcup_{i \in \mathbb{N}}
              \left( (C \cup C^{-1})^i
              \left( \bigcup_{j \in \mathbb{N}} (C \cup C^{-1})^j \right)
              \right) \\
          &= \bigcup_{i \in \mathbb{N}}
              \left(
                \bigcup_{j \in \mathbb{N}} (C \cup C^{-1})^i (C \cup C^{-1})^j
              \right) \\
          &= \bigcup_{i \in \mathbb{N}} \bigcup_{j \in \mathbb{N}}
              (C \cup C^{-1})^{i+j} \\
          &= \bigcup_{k \in \mathbb{N}} (C \cup C^{-1})^k = E
      \end{split}
    \]
    This shows that \(E \supseteq EE\), which means \(E\) is transitive. Since \(E\)
    fulfills the reflexive property, is symmetric and transitive, it is an
    equivalence relation.

    Next is to prove that every equivalence relation on \(S\) containing \(C\)
    contains \(E\). Let \(D\) be an equivalence relation on \(S\) containing \(C\). The
    idea of the proof is to split \(D\) into a known part \(D_i\) and an unknown
    part \(X_i\), where \(D_i\) and \(X_i\) are disjoint. Since \(D\) is an equivalence
    relation \(1_S \subseteq D\). Also \(C \subseteq D\) and the symmetry gives
    \(C^{-1} \subseteq D\). Thus initially \(D_0\) and \(X_0\) can be set to.
    \[ D_0 = 1_S \cup (C \cup C^{-1}) \]
    \[ X_0 = D \setminus D_0 \]
    The rest of the proof relies on iteration on the transitive property. The
    first iteration is written out before the concept is generalized. The
    transitivity can be written as
    \[ DD \subseteq D. \]
    Thus
    \[ (D_0 \cup X_0) (D_0 \cup X_0) \subseteq D. \]
    Using \eqref{5VxX} and \eqref{lm6W} this can be written as
    \[ D_0 D_0 \cup X_0 D_0 \cup D_0 X_0 \cup X_0 X_0 \subseteq D. \]
    Especially \(D_0 D_0 \subseteq D\). Next \(D_0 D_0\) is calculated.
    \[
      \begin{split}
        D_0 D_0 &= (1_S \cup (C \cup C^{-1})) (1_S \cup (C \cup C^{-1})) \\
                &= (1_S \cup (C \cup C^{-1})) 1_S \cup
                   (1_S \cup (C \cup C^{-1})) (C \cup C^{-1}) \\
                &= 1_S 1_S \cup (C \cup C^{-1}) 1_S \cup
                   1_S (C \cup C^{-1}) \cup (C \cup C^{-1}) (C \cup C^{-1})
      \end{split}
    \]
    Using the identity law proven in \ref{LN4u}, \(A \cup A = A\) and \(A A = A^2\)
    for any given set
    \[ D_0 D_0 = 1_S \cup (C \cup C^{-1}) \cup (C \cup C^{-1})^2 \]
    is acquired. It is known that \(D_0 D_0 \subseteq D\) which means the known
    part can be extended to \(D_0 D_0\), thus
    \[ D_1 = D_0 D_0 = 1_S \cup (C \cup C^{-1}) \cup (C \cup C^{-1})^2, \]
    \[ X_1 = D \setminus D_1. \]
    This can be generalized. Define \((C \cup C^{-1})^0 = 1_S\). Assume
    \[ D = D_n \cup X_n \]
    where
    \[ D_n = \bigcup_{k = 0}^{2^n} (C \cup C^{-1})^k \]
    and
    \[ X_n = D \setminus D_n. \]
    Then transitivity gives
    \[ D D \subseteq D \iff (D_n \cup X_n) (D_n \cup X_n) \subseteq D. \]
    Using \eqref{5VxX} and \eqref{lm6W} this is equal to
    \[ D_n D_n \cup X_n D_n \cup D_n X_n \cup X_n X_n \subseteq D. \]
    Especially
    \[ D_n D_n \subseteq D. \]
    Using the assumption this is the same as
    \[ \left( \bigcup_{i = 0}^{2^n} (C \cup C^{-1})^i \right)
       \left( \bigcup_{j = 0}^{2^n} (C \cup C^{-1})^j \right) \subseteq D. \]
    Using \eqref{5VxX} and \eqref{lm6W} this can be simplified.
    \[
      \begin{split}
        D_n D_n
          &=
            \left( \bigcup_{i = 0}^{2^n} (C \cup C^{-1})^i \right)
            \left( \bigcup_{j = 0}^{2^n} (C \cup C^{-1})^j \right) \\
          &=
            \bigcup_{i = 0}^{2^n} \left[ (C \cup C^{-1})^i
            \left( \bigcup_{j = 0}^{2^n} (C \cup C^{-1})^j \right) \right] \\
          &=
            \bigcup_{i = 0}^{2^n} \left( \bigcup_{j = 0}^{2^n}
            (C \cup C^{-1})^i (C \cup C^{-1})^j \right) \\
          &=
            \bigcup_{i = 0}^{2^n} \bigcup_{j = 0}^{2^n}
            (C \cup C^{-1})^{i+j} \\
          &=
            \bigcup_{k = 0}^{2^{n+1}} (C \cup C^{-1})^k
      \end{split}
    \]
    It is known that \(D_n D_n \subseteq D\), which means the known part can be
    extended to \(D_n D_n\), thus one gets
    \[ D = D_{n+1} \cup X_{n+1} \]
    where
    \[ D_{n+1} = D_n D_n = \bigcup_{k = 0}^{2^{n+1}} (C \cup C^{-1})^k \]
    and
    \[ X_{n+1} = D \setminus D_{n+1}. \]
    This proves that if the assumption is true for any \(n\) it is also true for
    \(n+1\) and since the assumption is true for \(n=0\) (and \(n=1\), aswell)
    induction proves that for all \(n \in \mathbb{N}\)
    \[ \bigcup_{k=0}^{2^n} (C \cup C^{-1})^k \subseteq D. \]
    Since it is true for all \(n\) the upper index can be thrown away and the
    result can be written as
    \[ E = \bigcup_{k \in \mathbb{N}} (C \cup C^{-1})^k \subseteq D. \]
    Q.E.D.
\end{enumerate}

\end{document}
